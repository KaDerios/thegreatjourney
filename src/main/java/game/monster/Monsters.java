package game.monster;

import java.util.Random;

public enum Monsters {
	FIRE_ELEMENTAL("Огненный элементал", 50, 120, 20, 10, "Огненный шар", 10),
	STONE_GOLEM("Каменный голем", 80, 70, 15, 10, "Каменные шипы", 7),
	SLIME("Слайм", 30, 10, 5, 3, "слизь", 7),
	CROW("Ворона", 40, 30, 10, 7, "Удар с высоты", 7),
	SNAKE("Змея", 65, 70, 20, 15, "Укус", 10);

	private static final String ANSI_RESET = "\u001B[0m";
	private static final String ANSI_RED = "\u001B[31m";
	private static final String ANSI_BLUE = "\u001B[34m";

	public final String name;
	public final Integer health;
	public final Integer mana;
	public final Integer damage;
	public final Integer defense;
	public final String skillName;
	public final Integer skillDamage;

	Monsters(String name, Integer health, Integer mana, Integer damage, Integer defense, String skillName, Integer skillDamage) {
		this.name = name;
		this.health = health;
		this.mana = mana;
		this.damage = damage;
		this.defense = defense;
		this.skillName = skillName;
		this.skillDamage = skillDamage;
	}

	public static Monsters getRandomMonster() {
		Random random = new Random();
		return values()[random.nextInt(values().length)];
	}
}
