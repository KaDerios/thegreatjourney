package game.monster;

public class Monster {
	private static final String ANSI_RESET = "\u001B[0m";
	private static final String ANSI_RED = "\u001B[31m";
	private static final String ANSI_BLUE = "\u001B[34m";

	private String name;
	private Integer maxHealth;
	private Integer health;
	private Integer mana;
	private Integer damage;
	private Integer defense;
	private String skillName;
	private Integer skillDamage;

	public Monster(Monsters monsters) {
		this.name = monsters.name;
		this.maxHealth = monsters.health;
		this.health = monsters.health;
		this.mana = monsters.mana;
		this.damage = monsters.damage;
		this.defense = monsters.defense;
		this.skillName = monsters.skillName;
		this.skillDamage = monsters.skillDamage;
	}

	Monster(String name, Integer health, Integer mana, Integer damage, Integer defense, String skillName, Integer skillDamage) {
		this.name = name;
		this.health = health;
		this.mana = mana;
		this.damage = damage;
		this.defense = defense;
		this.skillName = skillName;
		this.skillDamage = skillDamage;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getMaxHealth() {
		return maxHealth;
	}

	public void setMaxHealth(Integer maxHealth) {
		this.maxHealth = maxHealth;
	}

	public Integer getHealth() {
		return health;
	}

	public void setHealth(Integer health) {
		this.health = health;
	}

	public Integer getMana() {
		return mana;
	}

	public void setMana(Integer mana) {
		this.mana = mana;
	}

	public Integer getDamage() {
		return damage;
	}

	public void setDamage(Integer damage) {
		this.damage = damage;
	}

	public Integer getDefense() {
		return defense;
	}

	public void setDefense(Integer defense) {
		this.defense = defense;
	}

	public String getSkillName() {
		return skillName;
	}

	public void setSkillName(String skillName) {
		this.skillName = skillName;
	}

	public Integer getSkillDamage() {
		return skillDamage;
	}

	public void setSkillDamage(Integer skillDamage) {
		this.skillDamage = skillDamage;
	}

	public void bossUp() {
		name += " (Босс)";
		maxHealth *= 2;
		health = maxHealth;
		damage += 4;
		defense += 5;
		skillDamage += 6;
	}



	@Override
	public String toString() {
		String monster = ANSI_RED + name + ANSI_RESET + "\n";
		monster += "Макс. здоровье: " + maxHealth + "\n";
		monster += "Здоровье: " + health + "\n";
		monster += "Мана: " + mana + "\n";
		monster += "Урон: " + damage + "\n";
		monster += "Защита: " + defense + "\n";
		monster += ANSI_BLUE + skillName + ANSI_RESET + ": " + skillDamage + "\n";
		return monster;
	}
}
