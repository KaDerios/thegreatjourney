package game.items.clothes;

public class Clothes implements IClothes {

	public static final String ANSI_RESET = "\u001B[0m";
	public static final String ANSI_GREEN = "\u001B[32m";

	private String name;
	private String heroClass;
	private String type;
	private Integer health;
	private Integer mana;
	private Integer damage;
	private Integer defense;
	private Integer strange;
	private Integer dexterity;
	private Integer intellect;

	public Clothes() {
	}

	public Clothes(String name, String heroClass, String type, Integer health, Integer mana, Integer damage, Integer defense, Integer strange, Integer dexterity, Integer intellect) {
		this.name = name;
		this.heroClass = heroClass;
		this.type = type;
		this.health = health;
		this.mana = mana;
		this.damage = damage;
		this.defense = defense;
		this.strange = strange;
		this.dexterity = dexterity;
		this.intellect = intellect;
	}

	@Override
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String getHeroClass() {
		return heroClass;
	}

	public void setHeroClass(String heroClass) {
		this.heroClass = heroClass;
	}

	@Override
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public Integer getHealth() {
		return health;
	}

	public void setHealth(Integer health) {
		this.health = health;
	}

	@Override
	public Integer getMana() {
		return mana;
	}

	public void setMana(Integer mana) {
		this.mana = mana;
	}

	@Override
	public Integer getDamage() {
		return damage;
	}

	public void setDamage(Integer damage) {
		this.damage = damage;
	}

	@Override
	public Integer getDefense() {
		return defense;
	}

	public void setDefense(Integer defense) {
		this.defense = defense;
	}

	@Override
	public Integer getStrange() {
		return strange;
	}

	public void setStrange(Integer strange) {
		this.strange = strange;
	}

	@Override
	public Integer getDexterity() {
		return dexterity;
	}

	public void setDexterity(Integer dexterity) {
		this.dexterity = dexterity;
	}

	@Override
	public Integer getIntellect() {
		return intellect;
	}

	public void setIntellect(Integer intellect) {
		this.intellect = intellect;
	}

	@Override
	public String toString() {
		String clothes = ANSI_GREEN + name + ANSI_RESET + "\nТип: " + type + "\nТребуемый класс: " + heroClass + "\n";
		if(notEmpty(health)){
			clothes += "Здоровье: " + health + "\n";
		}
		if(notEmpty(mana)){
			clothes += "Мана: " + mana + "\n";
		}
		if(notEmpty(damage)){
			clothes += "Урон: " + damage + "\n";
		}
		if(notEmpty(defense)){
			clothes += "Защита: " + defense + "\n";
		}
		if(notEmpty(strange)){
			clothes += "Сила: " + strange + "\n";
		}
		if(notEmpty(dexterity)){
			clothes += "Ловкость: " + dexterity + "\n";
		}
		if(notEmpty(intellect)){
			clothes += "Интеллект: " + intellect + "\n";
		}
		return clothes;
	}

	private Boolean notEmpty(Integer param){
		return param != null && param > 0;
	}
}
