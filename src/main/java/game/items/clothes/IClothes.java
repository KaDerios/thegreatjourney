package game.items.clothes;

public interface IClothes {

	String getName();

	String getHeroClass();

	String getType();

	Integer getHealth();

	Integer getMana();

	Integer getDamage();

	Integer getDefense();

	Integer getStrange();

	Integer getDexterity();

	Integer getIntellect();
}
