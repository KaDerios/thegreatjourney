package game.items.clothes;

public enum ClothesType {
	HELMET,
	ARMOR,
	WEAPON
}
