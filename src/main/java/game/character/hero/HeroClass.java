package game.character.hero;

public enum HeroClass {
	MAGE("mage", 70, 120, 1111111, 5, 5, 5, 15, "Ледяная стрела", 10),
	WARRIOR("warrior", 120, 70, 15, 15, 15, 5, 5, "Удар быка", 10),
	THIEF("thief", 100, 70, 30, 10, 5, 15, 5, "Теневой клон", 10);

	public final String heroClass;
	public final Integer health;
	public final Integer mana;
	public final Integer damage;
	public final Integer defense;
	public final Integer strange;
	public final Integer dexterity;
	public final Integer intellect;
	public final String skillName;
	public final Integer skillDamage;

	HeroClass(String heroClass, Integer health, Integer mana, Integer damage, Integer defense, Integer strange, Integer dexterity, Integer intellect, String skillName, Integer skillDamage) {
		this.heroClass = heroClass;
		this.health = health;
		this.mana = mana;
		this.damage = damage;
		this.defense = defense;
		this.strange = strange;
		this.dexterity = dexterity;
		this.intellect = intellect;
		this.skillName = skillName;
		this.skillDamage = skillDamage;
	}
}
