package game.character.hero;

import game.character.ICharacter;
import game.items.clothes.Clothes;

import java.util.List;

public class Hero implements ICharacter {

	private static final String ANSI_RESET = "\u001B[0m";
	private static final String ANSI_YELLOW = "\u001B[33m";
	private static final String ANSI_BLUE = "\u001B[34m";;

	private String name;
	private Integer level;
	private String heroClass;
	private Integer maxHealth;
	private Integer health;
	private Integer mana;
	private Integer damage;
	private Integer defense;
	private Integer strange;
	private Integer dexterity;
	private Integer intellect;
	private String skillName;
	private Integer skillDamage;
	private List<Clothes> clothes;

	public Hero() {
	}

	public Hero(String name, Integer level, String heroClass, Integer health, Integer mana, Integer damage,
				Integer defense, Integer strange, Integer dexterity, Integer intellect, String skillName,
				Integer skillDamage, List<Clothes> clothes) {
		this.name = name;
		this.level = level;
		this.heroClass = heroClass;
		this.maxHealth = health;
		this.health = health;
		this.mana = mana;
		this.damage = damage;
		this.defense = defense;
		this.strange = strange;
		this.dexterity = dexterity;
		this.intellect = intellect;
		this.skillName = skillName;
		this.skillDamage = skillDamage;
		this.clothes = clothes;
	}

	@Override
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	@Override
	public String getHeroClass() {
		return heroClass;
	}

	public void setHeroClass(String heroClass) {
		this.heroClass = heroClass;
	}

	@Override
	public Integer getMaxHealth() {
		return maxHealth;
	}

	public void setMaxHealth(Integer maxHealth) {
		this.maxHealth = maxHealth;
	}

	@Override
	public Integer getHealth() {
		return health;
	}

	public void setHealth(Integer health) {
		this.health = health;
	}

	@Override
	public Integer getMana() {
		return mana;
	}

	public void setMana(Integer mana) {
		this.mana = mana;
	}

	@Override
	public Integer getDamage() {
		return damage;
	}

	public void setDamage(Integer damage) {
		this.damage = damage;
	}

	@Override
	public Integer getDefense() {
		return defense;
	}

	public void setDefense(Integer defense) {
		this.defense = defense;
	}

	@Override
	public Integer getStrange() {
		return strange;
	}

	public void setStrange(Integer strange) {
		this.strange = strange;
	}

	@Override
	public Integer getDexterity() {
		return dexterity;
	}

	public void setDexterity(Integer dexterity) {
		this.dexterity = dexterity;
	}

	@Override
	public Integer getIntellect() {
		return intellect;
	}

	public void setIntellect(Integer intellect) {
		this.intellect = intellect;
	}

	public String getSkillName() {
		return skillName;
	}

	public void setSkillName(String skillName) {
		this.skillName = skillName;
	}

	public Integer getSkillDamage() {
		return skillDamage;
	}

	public void setSkillDamage(Integer skillDamage) {
		this.skillDamage = skillDamage;
	}

	@Override
	public List<Clothes> getClothes() {
		return clothes;
	}

	public void setClothes(List<Clothes> clothes) {
		this.clothes = clothes;
	}

	@Override
	public String toString() {
		String hero = ANSI_YELLOW + name + ANSI_RESET + "\nУровень: " + level + "\nКласс: " + heroClass + "\n";
			hero += "Макс. здоровье: " + maxHealth + "\n";
			hero += "Здоровье: " + health + "\n";
			hero += "Мана: " + mana + "\n";
			hero += "Урон: " + damage + "\n";
			hero += "Защита: " + defense + "\n";
			hero += "Сила: " + strange + "\n";
			hero += "Ловкость: " + dexterity + "\n";
			hero += "Интеллект: " + intellect + "\n";
			hero += ANSI_BLUE + skillName + ANSI_RESET + ": " + skillDamage + "\n";
			hero += "Снаряжение: " + clothes + "\n";
		return hero;
	}
}
