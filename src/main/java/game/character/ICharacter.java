package game.character;

import game.items.clothes.Clothes;

import java.util.List;

public interface ICharacter {
	String getName();

	Integer getLevel();

	String getHeroClass();

	Integer getMaxHealth();

	Integer getHealth();

	Integer getMana();

	Integer getDamage();

	Integer getDefense();

	Integer getStrange();

	Integer getDexterity();

	Integer getIntellect();

	String getSkillName();

	Integer getSkillDamage();

	List<Clothes> getClothes();

}
