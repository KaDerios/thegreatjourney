package game.system;

public enum Skill {
	FIREBALL("Огненный шар", 10),
	;
	String name;
	Integer damage;

	Skill(String name, Integer damage) {
		this.name = name;
		this.damage = damage;
	}

	public String getName() {
		return name;
	}

	public Integer getDamage() {
		return damage;
	}
}
