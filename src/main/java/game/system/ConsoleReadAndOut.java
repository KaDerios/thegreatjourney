package game.system;

import java.util.Scanner;

public class ConsoleReadAndOut {

	private static final String ANSI_RESET = "\u001B[0m";
	private static final String ANSI_PURPLE = "\u001B[35m";
	private static final String ANSI_RED = "\u001B[31m";
	private static final String ANSI_GREEN = "\u001B[32m";
	private static final String ANSI_YELLOW = "\u001B[33m";
	private static final String ANSI_BLUE = "\u001B[34m";

	public String WIN = ANSI_RED + "ПО" + ANSI_RESET + ANSI_GREEN + "БЕ" + ANSI_RESET + ANSI_BLUE + "ДА" + ANSI_RESET + ANSI_PURPLE + "!" + ANSI_RESET;
	public String WRITE_NAME = "Введите имя";
	public String CHOOSE_CLASS = "Выберите класс (введите 'mage', 'warrior' или 'thief')";
	public String NEW_MONSTER = "Появляется монстр:\n";
	public String WRONG_COMMAND = "Неверная команда, попробуйте ещё раз";
	public String HERO_CREATE = ANSI_PURPLE + "Вы создали персонажа!" + ANSI_RESET + "\n";
	public String HERO_HEALING = "Вы вылечились, ваше здоровье: ";
	public String MONSTER_HEALING = "Монстр вылечился, его здоровье: ";
	public String HERO_ATTACK = "Вы нанесли урон: ";
	public String MONSTER_ATTACK = "Монстр нанёс урон: ";
	public String HERO_HEALTH = "Ваше здоровье: ";
	public String MONSTER_HEALTH = "Здоровье монстра: ";
	public String HERO_SKIP = "Вы пропустили ход";
	public String MONSTER_SKIP = "Монстр пропустил ход";
	public String YOU_DIED = ANSI_RED + "YOU DIED" + ANSI_RESET;
	public String MONSTER_DIED = ANSI_YELLOW + "Монстр побеждён" + ANSI_RESET;
	public String CHOOSE_WAY = "Выберите сторону направления движения ('middle', 'left' или 'right')";
	public String MIDDLE_WAY = "Вы пошли прямо";
	public String LEFT_WAY = "Вы пошли налево";
	public String RIGHT_WAY = "Вы пошли направо";
	public String CHOOSE_CLOTHES = "\nОставить вещь себе? (введите yes/no)";
	public String HERO_INFO = "Обновлённая информация о герое:\n";

	public String readLine() {
		Scanner in = new Scanner(System.in);
		return in.nextLine();
	}

	public void log(String text) {
		System.out.println(text);
	}
}
