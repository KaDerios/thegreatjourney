package game.system;

import game.character.hero.Hero;
import game.character.hero.HeroClass;
import game.items.clothes.Clothes;
import game.monster.Monster;
import game.monster.Monsters;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class SystemGame {

	private static final String ANSI_RESET = "\u001B[0m";
	private static final String ANSI_PURPLE = "\u001B[35m";
	private static final String ANSI_RED = "\u001B[31m";
	private static final String ANSI_GREEN = "\u001B[32m";
	private static final String ANSI_YELLOW = "\u001B[33m";
	private static final String ANSI_BLUE = "\u001B[34m";

	private final ThreadLocalRandom random = ThreadLocalRandom.current();
	private final ConsoleReadAndOut consoleReadAndOut = new ConsoleReadAndOut();
	private Hero hero;
	private Monster monster;
	private List<Clothes> clothesList;
	private List<String> nameClothesList;
	private List<String> typeClothesList;

	public void startGame() {
		int monsterCount = 0;
		newHero();
		do {
			checkWay();
			monster = new Monster(Monsters.getRandomMonster());
			fight();
			monsterCount++;
		} while (monsterCount < 3 && hero.getHealth() > 0);
		if (hero.getHealth() > 0) {
			monster.bossUp();
			fight();
		}
		if (hero.getHealth() > 0) {
			consoleReadAndOut.log(consoleReadAndOut.WIN);
		}
	}

	private void newHero() {
		boolean correctWord = false;
		consoleReadAndOut.log(consoleReadAndOut.WRITE_NAME);
		String name = consoleReadAndOut.readLine();
		String heroClass;
		do {
			consoleReadAndOut.log(consoleReadAndOut.CHOOSE_CLASS);
			heroClass = consoleReadAndOut.readLine();
			if ("mage".equals(heroClass) || "warrior".equals(heroClass) || "thief".equals(heroClass)) {
				correctWord = true;
			} else {
				consoleReadAndOut.log(consoleReadAndOut.WRONG_COMMAND);
			}
		} while (!correctWord);
		createNewCharacter(name, heroClass);
		consoleReadAndOut.log(consoleReadAndOut.HERO_CREATE + hero);
	}

	private void changeClothesList(Clothes clothes) {
		List<Clothes> temporaryClothesList = new ArrayList<>();
		for (Clothes changingClothes : clothesList) {
			if (clothes.getType().equals(changingClothes.getType())) {
				temporaryClothesList.add(clothes);
			} else {
				temporaryClothesList.add(changingClothes);
			}
		}
		clothesList = temporaryClothesList;
		hero.setClothes(clothesList);
	}

	private void getAction(String attackQueue) {
		getRandomAction(attackQueue);
	}

	private void getRandomAction(String attackQueue) {
		int chance = random.nextInt(100);
		if ("heroAttack".equals(attackQueue)) {
			if (hero.getHealth() < 30) {
				lowHpActions(chance, attackQueue);
			} else {
				goodHpActions(chance, attackQueue);
			}
		} else if ("monsterAttack".equals(attackQueue)) {
			if (monster.getHealth() < 20) {
				lowHpActions(chance, attackQueue);
			} else {
				goodHpActions(chance, attackQueue);
			}
		}
	}

	private void healing(String attackQueue) {
		if ("heroAttack".equals(attackQueue)) {
			if (hero.getMaxHealth() - hero.getHealth() > 50) {
				hero.setHealth(hero.getHealth() + 50);
				consoleReadAndOut.log(consoleReadAndOut.HERO_HEALING + ANSI_GREEN + hero.getHealth() + ANSI_RESET);

			} else {
				hero.setHealth(hero.getMaxHealth());
				consoleReadAndOut.log(consoleReadAndOut.HERO_HEALING + ANSI_GREEN + hero.getHealth() + ANSI_RESET);
			}
		}
		if ("monsterAttack".equals(attackQueue)) {
			if (monster.getMaxHealth() - monster.getHealth() > 35) {
				monster.setHealth(monster.getHealth() + 35);
				consoleReadAndOut.log(consoleReadAndOut.MONSTER_HEALING + ANSI_BLUE + monster.getHealth() + ANSI_RESET);
			} else {
				monster.setHealth(monster.getMaxHealth());
				consoleReadAndOut.log(consoleReadAndOut.MONSTER_HEALING + ANSI_BLUE + monster.getHealth() + ANSI_RESET);

			}
		}
	}

	private void calculateDamage(String attackQueue) {
		int attack;
		if ("heroAttack".equals(attackQueue)) {
			attack = hero.getDamage() + hero.getSkillDamage() - monster.getDefense();
			monster.setHealth(monster.getHealth() - attack);
			consoleReadAndOut.log(consoleReadAndOut.HERO_ATTACK + ANSI_YELLOW + attack + ANSI_RESET);
			consoleReadAndOut.log(ANSI_PURPLE + consoleReadAndOut.MONSTER_HEALTH + monster.getHealth() + ANSI_RESET);
		}
		if ("monsterAttack".equals(attackQueue)) {
			attack = monster.getDamage() + monster.getSkillDamage() - hero.getDefense();
			hero.setHealth(hero.getHealth() - attack);
			consoleReadAndOut.log(consoleReadAndOut.MONSTER_ATTACK + ANSI_RED + attack + ANSI_RESET);
			consoleReadAndOut.log(ANSI_BLUE + consoleReadAndOut.HERO_HEALTH + hero.getHealth() + ANSI_RESET);
		}
	}

	private void lowHpActions(Integer chance, String attackQueue) {
		if (chance < 40) {
			healing(attackQueue);
		} else if (chance < 80 && chance > 40) {
			calculateDamage(attackQueue);
		} else {
			if ("heroAttack".equals(attackQueue)) {
				consoleReadAndOut.log(consoleReadAndOut.HERO_SKIP);
			}
			if ("monsterAttack".equals(attackQueue)) {
				consoleReadAndOut.log(consoleReadAndOut.MONSTER_SKIP);
			}
		}
	}

	private void goodHpActions(Integer chance, String attackQueue) {
		if (chance < 20) {
			healing(attackQueue);
		} else if (chance < 70 && chance > 20) {
			calculateDamage(attackQueue);
		} else {
			if ("heroAttack".equals(attackQueue)) {
				consoleReadAndOut.log(consoleReadAndOut.HERO_SKIP);
			}
			if ("monsterAttack".equals(attackQueue)) {
				consoleReadAndOut.log(consoleReadAndOut.MONSTER_SKIP);
			}
		}
	}

	private void fight() {
		consoleReadAndOut.log(consoleReadAndOut.NEW_MONSTER + monster);
		while (true) {
			if (hero.getHealth() < 1) {
				consoleReadAndOut.log(consoleReadAndOut.YOU_DIED);
				break;
			} else {
				getAction("heroAttack");
			}
			if (monster.getHealth() < 1) {
				consoleReadAndOut.log(consoleReadAndOut.MONSTER_DIED);
				getRandomClothes();
				break;
			} else {
				getAction("monsterAttack");
			}
		}
	}

	private void checkWay() {
		boolean correctWord = false;
		do {
			consoleReadAndOut.log(consoleReadAndOut.CHOOSE_WAY);
			switch (consoleReadAndOut.readLine()) {
				case ("middle"):
					consoleReadAndOut.log(consoleReadAndOut.MIDDLE_WAY);
					correctWord = true;
					break;
				case ("left"):
					consoleReadAndOut.log(consoleReadAndOut.LEFT_WAY);
					correctWord = true;
					break;
				case ("right"):
					consoleReadAndOut.log(consoleReadAndOut.RIGHT_WAY);
					correctWord = true;
					break;
				default:
					consoleReadAndOut.log(consoleReadAndOut.WRONG_COMMAND);
					break;
			}
		} while (!correctWord);
	}

	private void getRandomClothes() {
		int index = random.nextInt(nameClothesList.size());
		Clothes clothes = null;
		String name = nameClothesList.get(index);
		index = random.nextInt(typeClothesList.size());
		String type = typeClothesList.get(index);
		switch (hero.getHeroClass()) {
			case ("mage"):
				clothes = new Clothes(name + " " + type, hero.getHeroClass(), type, random.nextInt(3), random.nextInt(10), random.nextInt(5),
						random.nextInt(3), random.nextInt(3), random.nextInt(3), random.nextInt(10));
				break;
			case ("warrior"):
				clothes = new Clothes(name + " " + type, hero.getHeroClass(), type, random.nextInt(10), random.nextInt(3), random.nextInt(5),
						random.nextInt(10), random.nextInt(10), random.nextInt(3), random.nextInt(3));
				break;
			case ("thief"):
				clothes = new Clothes(name + " " + type, hero.getHeroClass(), type, random.nextInt(5), random.nextInt(5), random.nextInt(5),
						random.nextInt(4), random.nextInt(3), random.nextInt(10), random.nextInt(3));
				break;
		}
		boolean correctWord = false;
		do {
			consoleReadAndOut.log(clothes + consoleReadAndOut.CHOOSE_CLOTHES);
			String answer = consoleReadAndOut.readLine();
			if ("yes".equals(answer)) {
				changeClothesList(clothes);
				consoleReadAndOut.log(consoleReadAndOut.HERO_INFO + hero);
				correctWord = true;
			} else if ("no".equals(answer)) {
				correctWord = true;
			} else {
				consoleReadAndOut.log(consoleReadAndOut.WRONG_COMMAND);
			}
		} while (!correctWord);
	}

	private void createNewCharacter(String name, String heroClass) {
		HeroClass character;
		autosetLists();
		if ("mage".equals(heroClass)) {
			character = HeroClass.MAGE;
			hero = new Hero(name, 1, character.heroClass, character.health, character.mana, character.damage,
					character.defense, character.strange, character.dexterity, character.intellect, character.skillName,
					character.skillDamage, clothesList);
		} else if ("warrior".equals(heroClass)) {
			character = HeroClass.WARRIOR;
			hero = new Hero(name, 1, character.heroClass, character.health, character.mana, character.damage,
					character.defense, character.strange, character.dexterity, character.intellect, character.skillName,
					character.skillDamage, clothesList);
		} else if ("thief".equals(heroClass)) {
			character = HeroClass.THIEF;
			hero = new Hero(name, 1, character.heroClass, character.health, character.mana, character.damage,
					character.defense, character.strange, character.dexterity, character.intellect, character.skillName,
					character.skillDamage, clothesList);
		}
	}

	private void autosetLists() {
		clothesList = new ArrayList<>();
		nameClothesList = new ArrayList<>();
		typeClothesList = new ArrayList<>();
		clothesList.add(new Clothes("Old helmet", "любой", "helmet", 1, 1, 0, 1, 1, 1, 1));
		clothesList.add(new Clothes("Old armor", "любой", "armor", 1, 1, 0, 1, 1, 1, 1));
		clothesList.add(new Clothes("Stick", "любой", "weapon", 0, 0, 1, 0, 1, 1, 1));
		nameClothesList.add("Ise cold");
		nameClothesList.add("Demonic");
		nameClothesList.add("Daedric");
		nameClothesList.add("Lemon");
		nameClothesList.add("Lollipop");
		typeClothesList.add("helmet");
		typeClothesList.add("armor");
		typeClothesList.add("weapon");
	}
}